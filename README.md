# Add Noise Drum (AND)

This is a drum module in Kosmo format intended for analog percussive sounds with tone and/or noise components: Kick drums, toms, wood blocks, hi-hats, snares. It has nine knob-controlled parameters to vary the sound along with two attenuated CV inputs. There are `SEND` and `RECEIVE` jacks for replacing or effecting the internal tone and noise sources, and a sensitivity control for the trigger input.

## Features and usage

Below is a block diagram of the module:

![](Images/and_block.png)

The circuit consists of five sections: Power (not shown), Trigger Handling, Tone, Noise, and Mixer. Tone consists of two decay envelope generators, a VCO, and a VCA; Noise comprises a noise source, two more decay envelope generators, a 6 db/octave VCF, and another VCA.

The Trigger Handling section just takes an input trigger or gate and conditions it into a trigger pulse of the height and width needed. A `SENSITIVITY` knob on the front panel adjusts the threshold at which the module is triggered, so if for example it is triggering on things that shouldn't trigger it you can raise the threshold.

The Tone section produces a tone pulse. The initial frequency is set by a front panel knob and by the `FREQ CV` input and its attenuator. The frequency drops from high to low (as is the case with many acoustic drums) with a time constant governed by the `FREQUENCY DECAY` knob, and the amount of drop is controlled by the `FREQUENCY DEPTH` knob. The loudness of the pulse decreases from its high initial value to zero with a time constant determined by the `AMPLITUDE DECAY` knob.

You can insert something like a filter or waveshaper between the VCO and the VCA by patching into it from the `SEND` jack with the result patched back into the `RECEIVE` jack. Or an external module's audio output can be plugged into the `RECEIVE` jack to substitute that output for the VCO as input to the VCA.

The Noise section behaves in a parallel way. The initial filter cutoff frequency is set by a front panel knob and by the `CUTOFF CV` input and its attenuator. The cutoff sweeps from high to low with a time constant governed by the `CUTOFF DECAY` knob and the amount of sweep is controlled by the `CUTOFF DEPTH` knob. The loudness of the pulse decreases from its high initial value to zero with a time constant determined by the `AMPLITUDE DECAY` knob.

As on the tone side, there are `SEND` and `RECEIVE` jacks between the noise source and the VCF. 

In the Mixer section, a `MIX` knob allows anything from 100% Tone through 50/50 Tone/Noise to 100% Noise to be sent to the `OUT` jack.

## Ancestry

Power, Trigger Handling, and Tone are based on the [Barton Analog Drum](https://bartonmusicalcircuits.com/drum/), with several modifications.

To that is added the Noise section (hence "Add Noise Drum") whose noise source is taken from the [MFOS Noise Cornucopia](https://musicfromouterspace.com/analogsynth_new/NOISECORNREV01/NOISECORNREV01.php) with added filtering to eliminate crosstalk. The VCF core comes from the LM13700 datasheet, with component changes. The envelope generators, VCF control current source, and VCA are identical to the ones in the Tone section.

The above modifications are discussed in more detail [here](Docs/changes.md).

## Modularity

The design has four PCBs: Two panel components boards and two main boards. The two panel components PCBs are identical, though assembled slightly differently. All four boards with a 100 mm panel constitute the Add Noise Drum. Just the Tone main board, one panel components board, and a 50 mm panel can be built as a Kosmo format Barton Analog Drum with the modifications mentioned above. The Tone `SEND` jack then becomes the module `OUT` jack, connected to the VCA output.

## Current draw
29 mA +12 V, 28 mA -12 V


## Photos

![](Images/and_assem.jpg)

## Status

Both Add Noise Drum and modified Analog Drum have been built and tested (though with cosmetically defective front panels, hence no photos yet). No problems found other than a few minor silkscreen deficiencies (noted in the build notes); these have been corrected in the design and Gerber files.

## Documentation

* [Schematic](Docs/and_schematic.pdf)
* PCB layout:   
    * [Tone main board front](Docs/Layout/and_ToneMainPCB/and_ToneMainPCB_front.svg)  
    * [Tone main board back](Docs/Layout/and_ToneMainPCB/and_ToneMainPCB_back.svg)  
    * [Noise main board front](Docs/Layout/and_NoiseMainPCB/and_NoiseMainPCB_front.svg)  
    * [Noise main board back](Docs/Layout/and_NoiseMainPCB/and_NoiseMainPCB_back.svg)  
    * [Panel components board front](Docs/Layout/and_PanelCompsPCB/and_PanelCompsPCB_front.svg)  
    * [Panel components board back](Docs/Layout/and_PanelCompsPCB/and_PanelCompsPCB_back.svg)
* [BOM](Docs/BOM/and_bom.md)
* [BOM grouped by PCB](Docs/BOM/and_bom_by_boards.md)
* [Blog post](https://richholmes.xyz/analogoutput/2024-01-03_add-noise-drum/)
* [Video](https://diode.zone/w/5uob3TnU9R7dDQK4VKM25v)

## Git repository

* [https://gitlab.com/rsholmes/and](https://gitlab.com/rsholmes/and)

