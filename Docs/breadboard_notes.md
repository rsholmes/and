# Add Noise Drum Breadboard notes

* With BMS specified 470Ω resistor from LM13700 pin 13 to ground, output at pin 9 is triangle wave going to ~ +10 V and clipped at -7 V. (Seen both on breadboard and on built Analog Drum module). With 1k instead, output is unclipped triangle wave ±5 V. But this also changes the frequency.

470R with 22 nF cap:

![](../Images/notes/470R_22nF.png)

(VCO output in yellow, VCA output in blue)

1k with 22 nF cap:

![](../Images/notes/1k_22nF.png)

1k with 44 nF (22||22) cap:

![](../Images/notes/1k_44nF.png)


* On breadboard and built Analog Drum module, VCA output saturates at ~ ±10 V until amplitude envelope is low enough. See scope images above. This gets better by increasing the 22k VCA input resistor, but asymmetrically: at ~ 90k the saturation is gone at the top but not at the bottom:

![](../Images/notes/90k.png)

At ~ 200k it is no longer saturated at the bottom:

![](../Images/notes/200k.png)

But that is with 470R and 22 nF on the oscillator. With 1k and 44 nF, saturation is gone by about 75k:

![](../Images/notes/75k_1k_44nF.png)

Note that 17 Vpp (470R) into 22k + 220R -> 168 mV, *way* above the 60 mV Electric Druid shoots for in their VCA design! 10 Vpp into 75k + 220R -> 30 mV, too low? Control voltage is up to 10 V. 10 V across 10k is 1 mA; ED designs for 500 µA. Here is: 20k instead of 10k for VCA CV; 39k for VCA signal; 1k for VCO resistor to ground; 44 nF for VCO capacitor:

![](../Images/notes/20k_39k_1k_44nF.png)

Provisionally I like it like this. (Actually use 47 nF for the cap of course.)

# Envelopes

With 4 envelopes, when one is turned down to minimum (200R to ground) it puts too big a load on the comparator and the amplitude of the trigger pulse going into all the envelopes is reduced. Needs about 5k minimum before the trigger pulse is essentially unaffected. (Biggest change is from 200R to 1k, diminishing returns from there.) Choices are to change 200R minimum to ~ 5k or to add a TL072 buffer on each board to share the load. I'll take the former.

# Noise source

I was not having success with the MFOS Noise Cornucopia Rev. 1 noise source. I was getting a high pitch tone in addition to the noise. I did two copies of the noise source on the breadboard and both had the same problem. Eventually I figured out the tone was in fact crosstalk from the VCO. The noise source output as viewed on the scope also showed glitches coincident with the module trigger input. I found adding a 47 µF capacitor from transistor base to ground (to filter the -12 V supply) eliminated both the VCO and trigger crosstalk.

# VCF cap and minimum CV

The Init Cutoff pot had a 470R resistor in series with it; this kept the filter from closing below audio frequency unless the cap was at least 3.3 nF, limiting the maximum cutoff. I can think of no reason why the cutoff CV can't go to 0 V. Eliminating the 470R resistor seems to work, and closes the VCF with a 1 nF (or indeed any) cap. In fact I don't know why the VCO CV can't go to 0 V too but I chose to keep the resistor on the tone side anyway.

