# Add Noise Drum build notes

Use two pots PCBs, one tone main PCB, one noise main PCB, and the Add Noise Drum panel. Assembly is the usual process aside from the following details.

## Main boards assembly

### Tone main PCB: 

* Fit Molex header J109. 
* Close one side of solder jumper JP201 as indicated on silkscreen. 

### Noise main PCB: 

* Cut off the collector of Q701 (2N3904) and leave that pad of the footprint empty. 
* Solder wires to pads J1203, J1204, and J1206 pads 1–3. 
* Fit a 5 position Molex connector to the other ends of these wires and plug it into J109 on the Tone main PCB. (Or omit the Molex header and solder the wires at both ends, of course.) See silkscreen labels to see what connects to what.

![](../Images/wires.png)

## Pots PCBs assembly

The pots PCBs for the tone side and the noise side are identical. However there are some slight differences in assembly. 

### Tone side pots board:

* Fit resistors R501 and R502.
* Use 470R for R505.
* Use an 11 position pin socket. Install it such that the two pads closest to the PCB corner, labeled TON and NSE, are empty and not used.

### Noise side pots board:

* Do not fit resistors R501 and R502 (leave those pads empty).
* Use a wire jumper in place of R505.
* Use a 13 position pin socket. All pads are occupied and used.

References on the PCB (5xx) correspond to the Tone side schematic. Noise side references are bigger by 600 (11xx).

You can use a Sharpie to mark one of the check boxes on the silkscreen to remind yourself which board is which.

## Mechanical assembly

Tone main PCB goes behind one pots PCB, connected via pin header/socket and one M3 spacer (I used a 10 mm one supplemented with three M3 nuts), behind the left side of the panel. Noise main PCB and its pots PCB, connected similarly, go behind the right side of the panel.

## Calibration

Connect the Noise side Send jack to an oscilloscope and adjust the trimmer until the noise is ~ ±5 V. Or just adjust the trimmer by ear to get a reasonable noise level.

## Analog Drum assembly

You can use one pots PCB (assembled like a tone side board, above), one tone main PCB, and the Analog Drum panel to build a modified version of the Barton Analog Drum. On the main PCB two things are done differently: 

* Omit J109. 
* Close other side of solder jumper JP201 as indicated on silkscreen.

## Errata

In the first run PCBs there are some silkscreen references not matching the present schematics:

| Silkscreen | Actual |
|----|----|
| R1 | R505 |
| J101 | J501 (Tone), J1101 (Noise)|
