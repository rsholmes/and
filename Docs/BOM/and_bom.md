# and.kicad_sch BOM

Tue 02 Jan 2024 10:07:08 AM EST

Generated from schematic by Eeschema 7.0.10-7.0.10~ubuntu22.04.1

**Component Count:** 153


## 
| Refs | Qty | Component | Description | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- |
| C101 | 1 | 10nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C201, C202, C206, C207, C701, C702, C709, C710 | 8 | 100nF | Ceramic disk capacitor, pitch 2.5 mm | Tayda | A-553 |  |
| C203 | 1 | 47nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C204, C205 | 2 | 10uF | Electrolytic capacitor, pitch 2.5 mm | Tayda | A-4349 |  |
| C301, C401, C801, C901 | 4 | 470nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C601, C1001 | 2 | 10uF | Unpolarized electrolytic capacitor, pitch 3.5 mm | Tayda |  |  |
| C703 | 1 | 1uF | Electrolytic capacitor, pitch 2.5 mm | Tayda |  |  |
| C704 | 1 | 47uF | Electrolytic capacitor, pitch 2.5 mm | Tayda |  |  |
| C705, C707 | 2 | 100nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C706 | 1 | 10pF | Ceramic disk capacitor, pitch 5 mm | Tayda |  |  |
| C708 | 1 | 1nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| D201–203, D301, D401, D701, D801, D901 | 8 | 1N4148 | Standard switching diode, DO-35 | Tayda | A-157 |  |
| D204, D205 | 2 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 | Tayda | A-159 |  |
| D501, D502, D1101, D1102 | 4 | LED_green | Light emitting diode | Tayda | A-1553 |  |
| J103 | 1 | Conn_01x11_Pin | 1x11 pin header, pitch 2.54 mm |  |  |  |
| J105, J202, J203, J701, J703, J1210 | 6 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |  |
| J109 | 1 | Molex hdr 5 pin | 5 position Molex header |  |  |  |
| J111, J1207 | 2 | DIP-16 | 16 pin DIP socket |  |  |  |
| J112, J1208 | 2 | DIP-14 | 14 pin DIP socket | Tayda | A-004 |  |
| J201, J702 | 2 | AudioJack2_SwitchT | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) | Tayda | A-1121 |  |
| J204 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |  |
| J501, J1101 | 2 | Conn_01x13_Socket | 1x13 pin socket, pitch 2.54 mm |  |  |  |
| J1202 | 1 | Conn_01x13_Pin | 1x13 pin header, pitch 2.54 mm |  |  |  |
| J1205 | 1 | Molex conn 5 pin | 5 position Molex connector |  |  |  |
| J1209 | 1 | DIP-8 | 8 pin DIP socket |  |  |  |
| Q201, Q601, Q702, Q1001 | 4 | 2N3906 | -0.2A Ic, -40V Vce, Small Signal PNP Transistor, TO-92 | Tayda | A-117 |  |
| Q701 | 1 | 2N3904 | Small Signal NPN Transistor, TO-92 | Tayda | A-111 |  |
| R101, R202–205, R704–706, R710, R713, R717, R1201 | 12 | 100k | Resistor | Tayda |  |  |
| R201, R207, R716, R1203 | 4 | 1k | Resistor | Tayda |  |  |
| R206, R208, R702, R707, R711, R712, R718 | 7 | 10k | Resistor | Tayda |  |  |
| R209, R301, R401, R606, R801, R901, R1006 | 7 | 4.7k | Resistor | Tayda |  |  |
| R501 | 1 | 100R | Resistor | Tayda |  |  |
| R502, R601, R1001 | 3 | 20k | Resistor | Tayda |  |  |
| R503, R504, R1103, R1104 | 4 | RL | Resistor | Tayda |  | Choose value for desired LED brightness |
| R505 | 1 | 470R | Resistor | Tayda |  |  |
| R602, R1002 | 2 | 39k | Resistor | Tayda |  |  |
| R603, R604, R1003, R1004 | 4 | 220R | Resistor | Tayda |  |  |
| R605, R1005 | 2 | 33k | Resistor | Tayda |  |  |
| R701, R703, R709 | 3 | 470k | Resistor | Tayda |  |  |
| R708 | 1 | 2M | Resistor | Tayda |  |  |
| R714, R715 | 2 | 620R | Resistor | Tayda |  |  |
| R1105 | 1 | 0R | Resistor | Tayda |  |  |
| RV501, RV1101 | 2 | B10k | 9 mm vertical potentiometer | Tayda |  |  |
| RV502–504, RV1102–1104 | 6 | B100k | 9 mm vertical potentiometer | Tayda |  |  |
| RV505, RV506, RV1105, RV1106 | 4 | A1M | 9 mm vertical potentiometer | Tayda |  |  |
| RV701 | 1 | 500k | Bourns 3362P trimmer | Tayda |  |  |
| U101, U1202 | 2 | TL074 | Quad operational amplifier, DIP-14 | Tayda | A-1138 |  |
| U201, U701 | 2 | LM13700 | Dual OTA, DIP-16 | Tayda | A-900 |  |
| U702 | 1 | TL072 | Dual operational amplifier, DIP-8 | Tayda | A-037 |  |
| ZKN101–106, ZKN1201–1206 | 12 | Knob_MF-A01 | Knob |  |  |  |
| ZNU101, ZNU1201 | 2 | Nut | Nut |  |  |  |
| ZSC101, ZSC1201 | 2 | Screw | Screw |  |  |  |
| ZSP101, ZSP1201 | 2 | Spacer | Spacer |  |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|100R|1|R501|
|1k|4|R201, R207, R716, R1203|
|10k|7|R206, R208, R702, R707, R711, R712, R718|
|100k|12|R101, R202–205, R704–706, R710, R713, R717, R1201|
|20k|3|R502, R601, R1001|
|2M|1|R708|
|220R|4|R603, R604, R1003, R1004|
|33k|2|R605, R1005|
|39k|2|R602, R1002|
|470R|1|R505|
|4.7k|7|R209, R301, R401, R606, R801, R901, R1006|
|470k|3|R701, R703, R709|
|620R|2|R714, R715|
|RL|4|R503, R504, R1103, R1104|
|0R|1|R1105|

