# and.kicad_sch BOM

Tue 02 Jan 2024 09:51:26 AM EST

Generated from schematic by Eeschema 7.0.10-7.0.10~ubuntu22.04.1

**Component Count:** 153


## Tone main board
| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C101 | 1 | 10nF | Polyester film capacitor pitch 5 mm | Tayda |  |
| C201, C202, C206, C207 | 4 | 100nF | Ceramic disk capacitor, pitch 2.5 mm | Tayda | A-553 |
| C203 | 1 | 47nF | Polyester film capacitor pitch 5 mm | Tayda |  |
| C204, C205 | 2 | 10uF | Electrolytic capacitor, pitch 2.5 mm | Tayda | A-4349 |
| C301, C401 | 2 | 470nF | Polyester film capacitor pitch 5 mm | Tayda |  |
| C601 | 1 | 10uF | Unpolarized electrolytic capacitor, pitch 3.5 mm | Tayda |  |
| D201–203, D301, D401 | 5 | 1N4148 | Standard switching diode, DO-35 | Tayda | A-157 |
| D204, D205 | 2 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 | Tayda | A-159 |
| J103 | 1 | Conn_01x11_Pin | 1x11 pin header, pitch 2.54 mm |  |  |
| J105, J202, J203 | 3 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J109 | 1 | Molex hdr 5 pin | 5 position Molex header |  |  |
| J111 | 1 | DIP-16 | 16 pin DIP socket |  |  |
| J112 | 1 | DIP-14 | 14 pin DIP socket | Tayda | A-004 |
| J201 | 1 | AudioJack2_SwitchT | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) | Tayda | A-1121 |
| J204 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |
| Q201, Q601 | 2 | 2N3906 | -0.2A Ic, -40V Vce, Small Signal PNP Transistor, TO-92 | Tayda | A-117 |
| R101, R202–205 | 5 | 100k | Resistor | Tayda |  |
| R201, R207 | 2 | 1k | Resistor | Tayda |  |
| R206, R208 | 2 | 10k | Resistor | Tayda |  |
| R209, R301, R401, R606 | 4 | 4.7k | Resistor | Tayda |  |
| R601 | 1 | 20k | Resistor | Tayda |  |
| R602 | 1 | 39k | Resistor | Tayda |  |
| R603, R604 | 2 | 220R | Resistor | Tayda |  |
| R605 | 1 | 33k | Resistor | Tayda |  |
| U101 | 1 | TL074 | Quad operational amplifier, DIP-14 | Tayda | A-1138 |
| U201 | 1 | LM13700 | Dual OTA, DIP-16 | Tayda | A-900 |
| ZKN101–106 | 6 | Knob_MF-A01 | Knob |  |  |
| ZNU101 | 1 | Nut | Nut |  |  |
| ZSC101 | 1 | Screw | Screw |  |  |
| ZSP101 | 1 | Spacer | Spacer |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|2|R201, R207|
|10k|2|R206, R208|
|100k|5|R101, R202–205|
|20k|1|R601|
|220R|2|R603, R604|
|33k|1|R605|
|39k|1|R602|
|4.7k|4|R209, R301, R401, R606|

----
## Tone pots board
| Refs | Qty | Component | Description | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- |
| D501, D502 | 2 | LED_green | Light emitting diode | Tayda | A-1553 |  |
| J501 | 1 | Conn_01x13_Socket | 1x13 pin socket, pitch 2.54 mm |  |  |  |
| R501 | 1 | 100R | Resistor | Tayda |  |  |
| R502 | 1 | 20k | Resistor | Tayda |  |  |
| R503, R504 | 2 | RL | Resistor | Tayda |  | Choose value for desired LED brightness |
| R505 | 1 | 470R | Resistor | Tayda |  |  |
| RV501 | 1 | B10k | 9 mm vertical potentiometer | Tayda |  |  |
| RV502–504 | 3 | B100k | 9 mm vertical potentiometer | Tayda |  |  |
| RV505, RV506 | 2 | A1M | 9 mm vertical potentiometer | Tayda |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|100R|1|R501|
|20k|1|R502|
|470R|1|R505|
|RL|2|R503, R504|

----
## Noise main board
| Refs | Qty | Component | Description | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- |
| C701, C702, C709, C710 | 4 | 100nF | Ceramic disk capacitor, pitch 2.5 mm | Tayda | A-553 |  |
| C703 | 1 | 1uF | Electrolytic capacitor, pitch 2.5 mm | Tayda |  |  |
| C704 | 1 | 47uF | Electrolytic capacitor, pitch 2.5 mm | Tayda |  |  |
| C705, C707 | 2 | 100nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C706 | 1 | 10pF | Ceramic disk capacitor, pitch 5 mm | Tayda |  |  |
| C708 | 1 | 1nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C801, C901 | 2 | 470nF | Polyester film capacitor pitch 5 mm | Tayda |  |  |
| C1001 | 1 | 10uF | Unpolarized electrolytic capacitor, pitch 3.5 mm | Tayda |  |  |
| D701, D801, D901 | 3 | 1N4148 | Standard switching diode, DO-35 | Tayda | A-157 |  |
| J701, J703, J1210 | 3 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |  |
| J702 | 1 | AudioJack2_SwitchT | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) | Tayda | A-1121 |  |
| J1202 | 1 | Conn_01x13_Pin | 1x13 pin header, pitch 2.54 mm |  |  |  |
| J1205 | 1 | Molex conn 5 pin | 5 position Molex connector |  |  |  |
| J1207 | 1 | DIP-16 | 16 pin DIP socket |  |  |  |
| J1208 | 1 | DIP-14 | 14 pin DIP socket | Tayda | A-004 |  |
| J1209 | 1 | DIP-8 | 8 pin DIP socket |  |  |  |
| Q701 | 1 | 2N3904 | Small Signal NPN Transistor, TO-92 | Tayda | A-111 |  |
| Q702, Q1001 | 2 | 2N3906 | -0.2A Ic, -40V Vce, Small Signal PNP Transistor, TO-92 | Tayda | A-117 |  |
| R701, R703, R709 | 3 | 470k | Resistor | Tayda |  |  |
| R702, R707, R711, R712, R718 | 5 | 10k | Resistor | Tayda |  |  |
| R704–706, R710, R713, R717, R1201 | 7 | 100k | Resistor | Tayda |  |  |
| R708 | 1 | 2M | Resistor | Tayda |  |  |
| R714, R715 | 2 | 620R | Resistor | Tayda |  |  |
| R716, R1203 | 2 | 1k | Resistor | Tayda |  |  |
| R801, R901, R1006 | 3 | 4.7k | Resistor | Tayda |  |  |
| R1001 | 1 | 20k | Resistor | Tayda |  |  |
| R1002 | 1 | 39k | Resistor | Tayda |  |  |
| R1003, R1004 | 2 | 220R | Resistor | Tayda |  |  |
| R1005 | 1 | 33k | Resistor | Tayda |  |  |
| RV701 | 1 | 500k | Bourns 3362P trimmer | Tayda |  |  |
| U701 | 1 | LM13700 | Dual OTA, DIP-16 | Tayda | A-900 |  |
| U702 | 1 | TL072 | Dual operational amplifier, DIP-8 | Tayda | A-037 |  |
| U1202 | 1 | TL074 | Quad operational amplifier, DIP-14 | Tayda | A-1138 |  |
| ZKN1201–1206 | 6 | Knob_MF-A01 | Knob |  |  |  |
| ZNU1201 | 1 | Nut | Nut |  |  |  |
| ZSC1201 | 1 | Screw | Screw |  |  |  |
| ZSP1201 | 1 | Spacer | Spacer |  |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|2|R716, R1203|
|10k|5|R702, R707, R711, R712, R718|
|100k|7|R704–706, R710, R713, R717, R1201|
|20k|1|R1001|
|2M|1|R708|
|220R|2|R1003, R1004|
|33k|1|R1005|
|39k|1|R1002|
|4.7k|3|R801, R901, R1006|
|470k|3|R701, R703, R709|
|620R|2|R714, R715|

----
## Noise pots board (ref nos. on silk are 500 lower, e.g. D501)
| Refs | Qty | Component | Description | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- |
| D1101, D1102 | 2 | LED_green | Light emitting diode | Tayda | A-1553 |  |
| J1101 | 1 | Conn_01x13_Socket | 1x13 pin socket, pitch 2.54 mm |  |  |  |
| R1103, R1104 | 2 | RL | Resistor | Tayda |  | Choose value for desired LED brightness |
| R1105 | 1 | 0R | Resistor | Tayda |  |  |
| RV1101 | 1 | B10k | 9 mm vertical potentiometer | Tayda |  |  |
| RV1102–1104 | 3 | B100k | 9 mm vertical potentiometer | Tayda |  |  |
| RV1105, RV1106 | 2 | A1M | 9 mm vertical potentiometer | Tayda |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|RL|2|R1103, R1104|
|0R|1|R1105|

