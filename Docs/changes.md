# Circuit changes

## Power, Trigger Handling, and Tone sections

The Power, Trigger Handling and Tone sections of the Add Noise Drum are based on the [Barton Analog Drum](https://bartonmusicalcircuits.com/drum/) with changes as follows:

### Variable threshold

When I built the Analog Drum I noticed sometimes I had trouble with the trigger input. If I used a gate, it would occasionally trigger on both the rising and falling edge. I never tracked down the reason for this but my guess is the gate had some ringing on the falling edge, and this provided enough of a rising voltage to trigger the module. Barton's trigger comparator has a fixed threshold of 120 mV, and I suspect this was just too low for the inputs I was giving it.

The [Thomas Henry Bass++](https://www.birthofasynth.com/Thomas_Henry/Pages/Bass_Plus.html) has a variable comparator threshold, adjusted using a `SENSITIVITY` knob on the front panel, and I chose to copy that for this module.

### Envelope generators

Original minimum resistance to ground in the envelope generators was 220R. With four envelopes, this makes too big a load on the comparator op amp. To keep the amplitude of the trigger signal unchanged requires minimum resistance be raised to 4.7k. This is still only 1% of maximum resistance using 500k pots, but I still went ahead and changed the pots to 1M and the caps to 470 nF. I think this gives short enough envelopes — the decay time range is 200:1 and the minimum time constant is 2.2 ms versus about 30 ms for the conditioned trigger pulse width. The alternative would be to add more op amps to buffer the trigger and share the load, which doesn't seem worthwhile.

### VCO changes

I found the Analog Drum VCO output was a triangle wave that went up to ~ +10 V and was clipped at ~ -7 V. It doesn't need to be so large. Increasing the 470R resistor from LM13700 pin 13 to ground to 1k resulted in ~ ±5 V with no clipping. It also increased the oscillator frequency, so I changed the capacitor from 22 nF to 47 nF to restore the original frequency range.

### VCO send/receive

A pair of jacks was added: One (`SEND`) connected to the VCO output and one (`RECEIVE`) connected to the VCA input. The latter is normalled to the VCO output.

With nothing plugged into `RECEIVE`, this gives the same behavior as the Analog Drum. But one can patch `SEND` to some external module such as a filter or waveshaper, and patch that module's output back to `RECEIVE`, to use the modified version of the VCO's output. One can even plug an entirely different audio signal source into `RECEIVE` to substitute it for the VCO.

### VCA changes

Without the VCO changes, the output of the VCA was saturating at ~ ±10 V until the envelope dropped low enough. With the VCO changes the saturation lasted a shorter time but was still present.

Increasing the signal input resistor from 22k improved this. Note that in the Electric Druid [LM13700 VCA design discussion](https://electricdruid.net/design-a-eurorack-vintage-vca-with-the-lm13700/) it is stated the distortion gets to about 8% at 100 mVpp input signal level, and they go for about 60 mVpp (2–3% distortion). With 17 Vpp and 22k input resistor (and 220R to ground) in the Analog Drum, the signal level is almost 170 mVpp! With the input reduced to 10 Vpp, a 39k input resistor gives us about 56 mVpp.

This still doesn't eliminate the output saturation, however. ED also recommends keeping the control current below 500 µA (mainly to suppress control voltage feedthrough). In the Barton design the 10k CV resistor with a 10 V CV gives about 930 µA. Replacing that with 20k gets it down to 465 µA, and the output is no longer saturated.

### Other changes

As usual Barton specifies 10R resistors on the voltage rails, and 10 nF bypass capacitors. I have changed these to voltage reversal protection Schottkys and 100 nF bypass caps.

### Kosmo Analog Drum

When building a Kosmo version of the Analog Drum, using the Tone circuit board and one panel controls board, all the above changes are included except that there is no `SEND` jack — it becomes the module `OUT` jack. The `RECEIVE` jack is relabeled `TONE IN` to reduce confusion.

## Noise source

The noise source is based on the one from the [MFOS Noise Cornucopia](https://musicfromouterspace.com/analogsynth_new/NOISECORNREV01/NOISECORNREV01.php). On the breadboard I was getting crosstalk from other parts of the circuit (specifically, the VCO and the trigger). I eliminated this by adding a 47 µF capacitor from transistor base to ground to filter the -12 V rail.

## VCF

The VCF core comes from the LM13700 datasheet. Some component values were changed for different signal levels and frequency range. The control current source was copied from the Tone section VCO.
